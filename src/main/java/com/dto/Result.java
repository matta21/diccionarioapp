package com.dto;

import java.util.List;


public class Result{
    public String id;
    public String language;
    public List<LexicalEntry> lexicalEntries;
    public String type;
    public String word;
    
    
    public List<LexicalEntry> getLexicalEntries() {
        return this.lexicalEntries; 
    } 
    
    public void setLexicalEntries(List<LexicalEntry> lexicalEntries) {
        this.lexicalEntries = lexicalEntries; 
    } 
}
