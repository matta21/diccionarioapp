package com.dto;

import java.util.List;


public class Sens{
    public List<String> definitions;
    public String id;
    public List<Note> notes;
    
    public List<String> getDefinitions() {
        return this.definitions;
    } 
    
    public void setDefinitions(List<String> definitions) {
        this.definitions = definitions;
    } 
}