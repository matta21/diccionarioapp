package com.dto;

import java.util.List;


public class Entry{
    public List<String> etymologies;
    public List<GrammaticalFeature> grammaticalFeatures;
    public List<Sens> senses;
    
    public List<Sens> getSenses() {
        return this.senses;
    } 
    
    public void setSenses(List<Sens> senses) {
        this.senses = senses;
    } 
}
