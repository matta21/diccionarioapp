package com.dto;

import java.util.List;


public class LexicalEntry{
    public List<Compound> compounds;
    public List<Derivative> derivatives;
    public List<Entry> entries;
    public String language;
    public LexicalCategory lexicalCategory;
    public String text;
    
    
    public List<Entry> getEntries() {
        return this.entries; 
    } 
    
    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    } 
}
