
package com.dto;

import java.util.ArrayList;
import java.util.List;


public class PalabraDTO {
      private String id;
  Metadata MetadataObject;
  List<Result> ResultObject;
  private String word;


 // Getter Methods 

  public String getId() {
    return id;
  }

  public Metadata getMetadata() {
    return MetadataObject;
  }

  public String getWord() {
    return word;
  }
  
  public List<Result> getResults() {
    return ResultObject;
  }
  
 // Setter Methods 

  public void setId( String id ) {
    this.id = id;
  }

  public void setMetadata( Metadata metadataObject ) {
    this.MetadataObject = metadataObject;
  }

  public void setWord( String word ) {
    this.word = word;
  }

  public void setResults( List<Result> resultObject ) {
    this.ResultObject = resultObject;
  }
}
