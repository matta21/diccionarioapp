package com.mycompany.diccionario;

import com.controller.dao.PalabrasJpaController;
import com.controller.entity.Palabras;
import com.dto.PalabraDTO;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.json.simple.*;
import java.util.concurrent.ThreadLocalRandom;


@Path("diccionario")
public class ApiDiccionarioOxford {
    
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response significado(@PathParam("idbuscar") String idbuscar){
    
        try {
            Client client = ClientBuilder.newClient();
            
            WebTarget myResource = client.target("https://od-api.oxforddictionaries.com:443/api/v2/entries/es/" + idbuscar);
            PalabraDTO palabradto = myResource.request(MediaType.APPLICATION_JSON).header("app_id", "5ef2864c").header("app_key", "d251fbf0fcb9de5eead8eccd315390a5").get(PalabraDTO.class);
            
            Palabras palabra1 = new Palabras();
            
            palabra1.setPalabra(palabradto.getWord());
            Date fecha = new Date();
            palabra1.setFecha(fecha.toString());
            Random rand = new Random();
            int upperbound = 1000;
        
            int intRandom = rand.nextInt(upperbound); 
        
            String s = String.valueOf(intRandom);
            palabra1.setIdPalabra(s);
            
            String definicion = (String) palabradto.getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().toString();
            palabra1.setDefinicion(definicion);
            
            PalabrasJpaController dao = new PalabrasJpaController();
            
            dao.create(palabra1);
            
            return Response.ok(200).entity(palabra1).build();
        } catch (Exception ex) {
            Logger.getLogger(ApiDiccionarioOxford.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }
    
   @GET
    @Path("/historial")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarClientes(){
        
     PalabrasJpaController dao = new PalabrasJpaController();
     
     List<Palabras> lista = dao.findPalabrasEntities();
     
     return Response.ok(200).entity(lista).build();
    }
}
