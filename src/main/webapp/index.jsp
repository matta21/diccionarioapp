<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Diccionario</title>
         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
 <body style="background-color:#F0E68C">
        <div class="container">
            <div class="row">
                <div class="col"></div>
                <div class="col-9" id="divPrincipal">
                    <h2 style="text-align: center;">Diccionario API Oxford</h2>
                    <h5 style="text-align: center;">Examen Final- Carlos Matta</h5>
                    <hr>
                    
                    
                      <form  name="form" action="ConsultaController" method="POST">
                         Ingrese una palabra:
                            <input type="text" id="idbuscar" name="idbuscar">

                            <button type="submit" name="accion" value="buscar" class="btn btn-success">Buscar</button>
                            <button type="submit" name="accion" value="listar" class="btn btn-success">Resultados anteriores</button>
                            
                             <hr>
                             <h6> Repositorio bitbucket https://cmatta2021@bitbucket.org/matta21/diccionarioapp.git </h6>
                             <h6> Link de app heroku https://app-diccionarioxford.herokuapp.com/</h6>
                             <h6> fuente API https://developer.oxforddictionaries.com/ </h6>
                    </form>
                  </div>
                <div class="col"></div>
            </div>
        </div>
    </body>
</html>
