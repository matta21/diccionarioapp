<%@page import="java.util.Iterator"%>
<%@page import="com.controller.entity.Palabras"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Palabras> palabras = (List<Palabras>) request.getAttribute("listaPalabras");
    Iterator<Palabras> itPalabras = palabras.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Historial</title>
          <style>
                table {
                    width:100%;
                }
                table, th, td {
                     border: 1px solid black;
                     border-collapse: collapse;
                }
                th, td {
                     padding: 15px;
                    text-align: left;
                }
                #t01 tr:nth-child(even) {
                    background-color: #eee;
                }
                #t01 tr:nth-child(odd) {
                    background-color: #fff;
                }
                 #t01 th {
                    background-color: black;
                    color: white;
  }
  </style>
    </head>
    <body style="background-color:#F0E68C">
        <h1>Historial</h1>
         <form  name="form" action="ConsultaController" method="POST">

            <table id="t01" border="3">
                    <thead>
                    <th>ID Palabra</th>
                    <th>Palabra</th>
                    <th>Significado</th>
                    <th>Fecha</th>
         
                    </thead>
                    <tbody>
                        <%while (itPalabras.hasNext()) {
                       Palabras cm = itPalabras.next();%>
                        <tr>
                            <td id="id_palabra"><%= cm.getIdPalabra()%></td>
                            <td id="palabra"><%= cm.getPalabra()%></td>
                            <td id="definicion"><%= cm.getDefinicion()%></td>
                            <td id="fecha"><%= cm.getFecha()%></td>
                         
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
                    <br><br>
     
            <button type="submit" name="accion" value="volver" class="btn btn-success">Atras</button>
        
         </form>   
    </body>
